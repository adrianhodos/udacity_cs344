#ifndef TUPLE_TYPES_HPP__
#define TUPLE_TYPES_HPP__

#include <cuda.h>

template<int components, typename base_type>
struct tuple_type;

template<>
struct tuple_type<2, float> {
    typedef float2              type;

    static __device__ __host__ type make(float x, float y) {
        return make_float2(x, y);
    }
};

template<>
struct tuple_type<2, int> {
    typedef int2                type;

    static __device__ __host__ type make(int x, int y) {
        return make_int2(x, y);
    }
};

template<>
struct tuple_type<2, char> {
    typedef char2              type;

    static __device__ __host__ type make(char x, char y) {
        return make_char2(x, y);
    }
};

template<>
struct tuple_type<2, unsigned char> {
    typedef uchar2              type;

    static __device__ __host__ type make(unsigned char x, unsigned char y) {
        return make_uchar2(x, y);
    }
};

#endif /* !TUPLE_TYPES_HPP__ */
