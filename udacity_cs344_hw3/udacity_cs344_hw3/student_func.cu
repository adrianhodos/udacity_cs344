/* Udacity Homework 3
   HDR Tone-mapping

  Background HDR
  ==============

  A High Definition Range (HDR) image contains a wider variation of intensity
  and color than is allowed by the RGB format with 1 byte per channel that we
  have used in the previous assignment.  

  To store this extra information we use single precision floating point for
  each channel.  This allows for an extremely wide range of intensity values.

  In the image for this assignment, the inside of church with light coming in
  through stained glass windows, the raw input floating point values for the
  channels range from 0 to 275.  But the mean is .41 and 98% of the values are
  less than 3!  This means that certain areas (the windows) are extremely bright
  compared to everywhere else.  If we linearly map this [0-275] range into the
  [0-255] range that we have been using then most values will be mapped to zero!
  The only thing we will be able to see are the very brightest areas - the
  windows - everything else will appear pitch black.

  The problem is that although we have cameras capable of recording the wide
  range of intensity that exists in the real world our monitors are not capable
  of displaying them.  Our eyes are also quite capable of observing a much wider
  range of intensities than our image formats / monitors are capable of
  displaying.

  Tone-mapping is a process that transforms the intensities in the image so that
  the brightest values aren't nearly so far away from the mean.  That way when
  we transform the values into [0-255] we can actually see the entire image.
  There are many ways to perform this process and it is as much an art as a
  science - there is no single "right" answer.  In this homework we will
  implement one possible technique.

  Background Chrominance-Luminance
  ================================

  The RGB space that we have been using to represent images can be thought of as
  one possible set of axes spanning a three dimensional space of color.  We
  sometimes choose other axes to represent this space because they make certain
  operations more convenient.

  Another possible way of representing a color image is to separate the color
  information (chromaticity) from the brightness information.  There are
  multiple different methods for doing this - a common one during the analog
  television days was known as Chrominance-Luminance or YUV.

  We choose to represent the image in this way so that we can remap only the
  intensity channel and then recombine the new intensity values with the color
  information to form the final image.

  Old TV signals used to be transmitted in this way so that black & white
  televisions could display the luminance channel while color televisions would
  display all three of the channels.
  

  Tone-mapping
  ============

  In this assignment we are going to transform the luminance channel (actually
  the log of the luminance, but this is unimportant for the parts of the
  algorithm that you will be implementing) by compressing its range to [0, 1].
  To do this we need the cumulative distribution of the luminance values.

  Example
  -------

  input : [2 4 3 3 1 7 4 5 7 0 9 4 3 2]
  min / max / range: 0 / 9 / 9

  histo with 3 bins: [4 7 3]

  cdf : [4 11 14]


  Your task is to calculate this cumulative distribution by following these
  steps.

*/


#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <cstdio>
#include <cstdlib>
#include <memory>

#include "tuple_types.hpp"

void referenceCalculation(const float* const h_logLuminance, unsigned int* const h_cdf,
                          const size_t numRows, const size_t numCols, const size_t numBins);

#include "utils.h"

struct LuminanceHistogram {
    __device__ __host__ LuminanceHistogram(
        const float min_lum, const float lum_range, const unsigned int num_bins
        )
        :       num_bins_(num_bins)
            ,   min_luminance_(min_lum)
            ,   luminance_range_(lum_range)
    {}

    __device__ __host__ int operator()(const float input_luminance) const {
        const int output_bin = static_cast<int>(((input_luminance - min_luminance_) 
                                                  / luminance_range_) * num_bins_);
        return min(num_bins_ - 1, output_bin);
    }

    unsigned int        num_bins_;
    float               min_luminance_;
    float               luminance_range_;
};

__device__ float    partial_output_min[1024];
__device__ float    partial_output_max[1024];
__device__ int      g_histogram_bins[4096] = { 0 };

struct Reduction2Context_t {
    int     block_counter;
    float*  partial_output0;
    float*  partial_output1;
    float   final_output0;
    float   final_output1;
    float   output_range;

    __device__ __host__ void reduction_finished(
        const float output0, 
        const float output1
        ) {
        final_output0   = output0;
        final_output1   = output1;
        output_range    = final_output1 - final_output0;
    }
};

__device__ Reduction2Context_t g_reduce2_context = { 
    0, &partial_output_min[0], &partial_output_max[0], 0.0f, 0.0f, 0.0f 
};

template<typename T, typename ReduceOp>
__device__ T warp_reduce(volatile T* data, const int tid, ReduceOp reduce__) {
    data[tid] = reduce__(data[tid], data[tid + 32]);
    data[tid] = reduce__(data[tid], data[tid + 16]);
    data[tid] = reduce__(data[tid], data[tid + 8]);
    data[tid] = reduce__(data[tid], data[tid + 4]);
    data[tid] = reduce__(data[tid], data[tid + 2]);
    data[tid] = reduce__(data[tid], data[tid + 1]);
    return data[tid];
}

template<int BlockSize, typename T, typename ReduceOp0, typename ReduceOp1>
__device__ typename tuple_type<2, T>::type thread_reduce2_final_step(
    const T* __restrict__ input_data0,
    const T* __restrict__ input_data1,
    const int tid,
    const int array_size,
    ReduceOp0 reduction0__,
    ReduceOp1 reduction1__
    ) {
    typedef typename tuple_type<2, T>::type                  output_type_t;

    int load_addr = tid;

    output_type_t output_value;
    output_value.x = ReduceOp0::identity();
    output_value.y = ReduceOp1::identity();

    while (load_addr < array_size) {
        const T input_val0 = input_data0[load_addr];
        const T input_val1 = input_data1[load_addr];

        output_value.x = reduction0__(output_value.x, input_val0);
        output_value.y = reduction1__(output_value.y, input_val1);

        load_addr += BlockSize;
    }

    return output_value;
}

template<int BlockSize, typename T, typename ReduceOp0, typename ReduceOp1>
__device__ typename tuple_type<2, T>::type thread_reduce2(
    const T* __restrict__ input_data, 
    const int tid, 
    const int array_size,
    ReduceOp0 reduction0__,
    ReduceOp1 reduction1__
    ) {
    typedef typename tuple_type<2, T>::type    output_type_t;

    const int   grid_size     = BlockSize * 2 * gridDim.x;
    int         load_addr     = blockIdx.x * 2 * BlockSize + tid;

    output_type_t output_value;
    output_value.x = ReduceOp0::identity();
    output_value.y = ReduceOp1::identity();

    while (load_addr < array_size) {
        const T inputval0   =   input_data[load_addr];
        const T inputval1   =   input_data[load_addr + BlockSize];

        output_value.x = reduction0__(output_value.x, inputval0);
        output_value.y = reduction1__(output_value.y, inputval0);

        output_value.x = reduction0__(output_value.x, inputval1);
        output_value.y = reduction1__(output_value.y, inputval1);

        load_addr += grid_size;
    }

    return output_value;
}

template<int BlockSize, typename T, typename ReduceOp0, typename ReduceOp1>
__device__ typename tuple_type<2, T>::type block_reduce2(
    T* __restrict__ shmem_data0,
    T* __restrict__ shmem_data1,
    const int tid,
    ReduceOp0 reduction0__,
    ReduceOp1 reduction1__
    ) {
    typedef tuple_type<2, T>                    tuple_t;
    typedef typename tuple_type<2, T>::type     output_type_t;

    for (int offset = BlockSize >> 1; offset > 32; offset >>= 1) {
        if (tid < offset) {
            shmem_data0[tid] = reduction0__(shmem_data0[tid], 
                                            shmem_data0[tid + offset]);

            shmem_data1[tid] = reduction1__(shmem_data1[tid], 
                                            shmem_data1[tid + offset]);
        }
        __syncthreads();
    }

    if (tid < 32) {
        warp_reduce(shmem_data0, tid, reduction0__);
        warp_reduce(shmem_data1, tid, reduction1__);
    }

    return tuple_t::make(shmem_data0[tid], shmem_data1[tid]);
}

template<int BlockSize, typename T, typename ReduceOp0, typename ReduceOp1>
__global__ void reduction2(
    const T* __restrict__ input_data,
    const int num_elements,
    ReduceOp0 reduce_op0__,
    ReduceOp1 reduce_op1__,
    Reduction2Context_t* rcontext
    ) {
    const int tid = threadIdx.x;

    typedef typename tuple_type<2, T>::type          output_type_t;

    //
    // Perform per thread, serial reduction.
    output_type_t reduce_val = thread_reduce2<BlockSize>(input_data,
                                                         tid,
                                                         num_elements,
                                                         reduce_op0__,
                                                         reduce_op1__);

    extern __shared__   T       block_data[];
    __shared__          int     is_last_block_done;

    //
    // Deposit reduced values in shared memory.
    block_data[tid]                 = reduce_val.x;
    block_data[tid + BlockSize]     = reduce_val.y;

    __syncthreads();

    //
    // Perform block and warp level reduction.
    reduce_val = block_reduce2<BlockSize>(&block_data[0],
                                          &block_data[BlockSize],
                                          tid,
                                          reduce_op0__,
                                          reduce_op1__);    

    //
    // First thread writes output values and updates the global counter.
    if (tid == 0) {
        rcontext->partial_output0[blockIdx.x] = block_data[0];
        rcontext->partial_output1[blockIdx.x] = block_data[BlockSize];

        //
        // This fence is needed so that all write to global memory made by this
        // thread are visible to all grid threads.
        __threadfence();

        //
        // Increment block counter and see if this is the last block to 
        // finish execution.
        const int old_counter_value = atomicAdd(
            &rcontext->block_counter, 1
            );
        is_last_block_done = (old_counter_value == (gridDim.x - 1));
    }

    //
    // This is needed so that every thread sees the correct value for
    // is_last_block_done.
    __syncthreads();

    //
    // Last block to finish performs final reductions.
    if (is_last_block_done) {
        reduce_val  = thread_reduce2_final_step<BlockSize>(
            rcontext->partial_output0,
            rcontext->partial_output1,
            tid,
            gridDim.x,
            reduce_op0__,
            reduce_op1__
            );

        block_data[tid]             = reduce_val.x;
        block_data[tid + BlockSize] = reduce_val.y;

        __syncthreads();

        reduce_val = block_reduce2<BlockSize>(&block_data[0],
                                              &block_data[BlockSize],
                                              tid,
                                              reduce_op0__,
                                              reduce_op1__);

        if (tid == 0) {
            //
            // Write final output values
            rcontext->reduction_finished(reduce_val.x, reduce_val.y);
        }
    }
}

template<typename T, typename U, typename HistogramOperator>
__device__ void histogram_update(
    const T* __restrict__ input_items,
    const int num_items,
    U* __restrict__ shmem_bins,
    HistogramOperator histogram_op__
    ) {
    for (int i = 0; i < num_items; ++i) {
        const int dest_bin = histogram_op__(input_items[i]);
        atomicAdd(&shmem_bins[dest_bin], 1);
    }
}

template<int BlockSize, typename T, typename U>
__global__ void histogram(
    const T* __restrict__ input_data,
    const int num_elements,
    const int num_bins,
    U* __restrict__ output_bins
    ) {

    const int tid           = threadIdx.x;
    int array_global_offset = blockIdx.x * BlockSize + tid;
    const int kGridSize     = gridDim.x * BlockSize;

    const float kMinLuminance = g_reduce2_context.final_output0;
    const float kLuminanceRange = g_reduce2_context.output_range;

    T loaded_items[4];
    extern __shared__ U block_lvl_bins[];

    for (int i = tid; i < num_bins; i += BlockSize) {
        block_lvl_bins[i] = static_cast<U>(0);
    }

    __syncthreads();

    LuminanceHistogram lum_histo__(kMinLuminance, kLuminanceRange, num_bins);
    for ( ; array_global_offset < num_elements; ) {

        int i = 0;
        while ((i < 4) && (array_global_offset < num_elements)) {
            loaded_items[i] = input_data[array_global_offset];
            array_global_offset += kGridSize;
            ++i;
        }

        histogram_update(loaded_items, i, block_lvl_bins, lum_histo__);
    }

    __syncthreads();

    int tid_offset      = tid;
    while (tid_offset < num_bins) {
        atomicAdd(output_bins + tid_offset, block_lvl_bins[tid_offset]);
        tid_offset += BlockSize;
    }
}

const int num_banks     = 32;
const int log_num_banks = 5;

__device__ inline int conflict_free_offset(const int off) {
    return off >> log_num_banks;
}

template<int item_count, typename T, typename ScanOp>
__device__ inline void inclusive_scan(T* data_array, ScanOp scan_op__) {
    for (int i = 1; i < item_count; ++i) {
        data_array[i] = scan_op__(data_array[i], data_array[i - 1]);
    }
}

template
<
    int     BlockSize,
    int     WorkItemsPerThread,
    typename T,
    typename U,
    typename ScanOp
>
__global__ 
void prefix_scan(
    const T* input_data,
    ScanOp   scan_op__,
    U* output_data
    ) {
    const int tx                  = threadIdx.x;
    const int global_mem_offset   = blockIdx.x * BlockSize * WorkItemsPerThread;
    const int kHalfWorkPerThread  = WorkItemsPerThread >> 1;

    T local_input0[kHalfWorkPerThread];
    T local_input1[kHalfWorkPerThread];

    for (int i = 0; i < kHalfWorkPerThread; ++i) {
        local_input0[i] = 
          input_data[global_mem_offset + tx * WorkItemsPerThread + i];
    }

    for (int i = kHalfWorkPerThread; i < WorkItemsPerThread; ++i) {
        local_input1[i - kHalfWorkPerThread] = 
            input_data[global_mem_offset + tx * WorkItemsPerThread + i];
    }

    //
    // Make inplace inclusive scans for the local arrays.
    inclusive_scan<kHalfWorkPerThread>(&local_input0[0], scan_op__);
    inclusive_scan<kHalfWorkPerThread>(&local_input1[0], scan_op__);

    //
    // Store inclusive scan sums into shared memory.
    extern __shared__ T block_scan[];
    int ai = tx * 2 + 0;
    int bi = tx * 2 + 1;
    block_scan[ai + conflict_free_offset(ai)] = local_input0[kHalfWorkPerThread - 1];
    block_scan[bi + conflict_free_offset(bi)] = local_input1[kHalfWorkPerThread - 1];
    //
    // Perform exclusive scan of the partial sums stored in shared memory.
    int offset = 1;
    //
    // Upwards sweep pass.
    for (int depth = BlockSize; depth > 0; depth >>= 1) {
        __syncthreads();

        if (tx < depth) {
            int a_idx = offset * (2 * tx + 1) - 1;
            int b_idx = offset * (2 * tx + 2) - 1;
            a_idx += conflict_free_offset(a_idx);
            b_idx += conflict_free_offset(b_idx);

            block_scan[b_idx] = scan_op__(block_scan[b_idx], block_scan[a_idx]);
        }
        offset <<= 1;
    }

    //
    // Insert identity for the last element.
    if (tx == 0) {
        int idx = BlockSize * 2 - 1;
        block_scan[idx + conflict_free_offset(idx)] = ScanOp::identity();
    }

    //
    // Downwards sweep pass.
    for (int depth = 1; depth < (BlockSize << 1); depth <<= 1) {
        offset >>= 1;

        __syncthreads();

        if (tx < depth) {
            int a_idx = offset * (2 * tx + 1) - 1;
            int b_idx = offset * (2 * tx + 2) - 1;
            a_idx += conflict_free_offset(a_idx);
            b_idx += conflict_free_offset(b_idx);

            T temp_val          = block_scan[a_idx];
            block_scan[a_idx]   = block_scan[b_idx];
            block_scan[b_idx]   = scan_op__(block_scan[b_idx], temp_val);
        }
    }

    __syncthreads();

    //
    // Build final sum arrays.
    T accumulator = block_scan[ai + conflict_free_offset(ai)];
    for (int i = kHalfWorkPerThread - 1; i > 0; --i) {
        local_input0[i] = scan_op__(local_input0[i - 1], accumulator);
    }
    local_input0[0] = accumulator;

    accumulator = block_scan[bi + conflict_free_offset(bi)];
    for (int i = kHalfWorkPerThread - 1; i > 0; --i) {
        local_input1[i] = scan_op__(local_input1[i - 1], accumulator);
    }
    local_input1[0] = accumulator;

    //
    // Write values to global memory
    for (int i = 0; i < kHalfWorkPerThread; ++i) {
        output_data[global_mem_offset + tx * WorkItemsPerThread + i] = 
            local_input0[i];
    }

    for (int i = kHalfWorkPerThread; i < WorkItemsPerThread; ++i) {
        output_data[global_mem_offset + tx * WorkItemsPerThread + i] =
            local_input1[i - kHalfWorkPerThread];
    }
}

template<typename T>
struct min_func {
    __device__ __host__ T operator()(const T left, const T right) const {
        return min(left, right);
    }

    static __device__ __host__ T identity() {
        return FLT_MAX;
    }
};

template<typename T>
struct max_func {
    __device__ __host__ T operator()(const T left, const T right) const {
        return max(left, right);
    }

    static __device__ __host__ T identity() {
        return FLT_MIN;
    }
};

struct cuda_storage {
    void operator()(void* cuda_mem_ptr) const {
        cudaFree(cuda_mem_ptr);
    }
};

template<typename T>
struct sum_operator {
    __device__ __host__ static T identity() {
        return static_cast<T>(0);
    }

    __device__ __host__ T operator()(T lhs, T rhs) const {
        return lhs + rhs;
    }
};

void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins)
{
  //TODO
  /*Here are the steps you need to implement
    1) find the minimum and maximum value in the input logLuminance channel
       store in min_logLum and max_logLum
    2) subtract them to find the range
    3) generate a histogram of all the values in the logLuminance channel using
       the formula: bin = (lum[i] - lumMin) / lumRange * numBins
    4) Perform an exclusive scan (prefix sum) on the histogram to get
       the cumulative distribution of luminance values (this should go in the
       incoming d_cdf pointer which already has been allocated for you)       */

    printf("\nRows %u, cols %u, total bytes %u", numRows, numCols,
           numRows * numCols * sizeof(float));
    printf("\nBin count : %u", numBins);

    Reduction2Context_t* sym_context;
    cudaGetSymbolAddress((void**) &sym_context, g_reduce2_context);
    size_t sym_size;
    cudaGetSymbolSize(&sym_size, g_reduce2_context);

    int* sym_histogram_bins;
    cudaGetSymbolAddress((void**) &sym_histogram_bins, g_histogram_bins);

    {
        const int kNumthreadsPerBlock           = 256;
        const int kNumBlocks                    = 64;
        const int kNumBytesSharedMem            = kNumthreadsPerBlock * 2 * sizeof(float);
        const int kNumElements                  = static_cast<int>(numRows * numCols);
        const dim3 kGridSize(kNumBlocks);
        const dim3 kBlockSize(kNumthreadsPerBlock);

        reduction2
        <
            kNumthreadsPerBlock
        >
        <<<
            kGridSize,
            kBlockSize,
            kNumBytesSharedMem
        >>>
        (
            d_logLuminance, 
            kNumElements,
            min_func<float>(),
            max_func<float>(),
            sym_context
        );

        Reduction2Context_t output_ctx;
        cudaMemcpyFromSymbol(&output_ctx, g_reduce2_context, sym_size);

        min_logLum = output_ctx.final_output0;
        max_logLum = output_ctx.final_output1;
    }

    {
        assert(numBins == 1024);
        const int kNumThreadsPerBlock   = 512;
        const int kNumBlocks            = 64;
        const int shared_mem_per_block  = numBins * sizeof(int);
        
        const dim3 grid_size(kNumBlocks);
        const dim3 block_size(kNumThreadsPerBlock);

        histogram
        <
            kNumThreadsPerBlock
        >
        <<<
            grid_size,
            block_size,
            shared_mem_per_block
        >>>
        (
            d_logLuminance,
            static_cast<int>(numRows * numCols),
            static_cast<int>(numBins),
            sym_histogram_bins
        );
    }

    {
        const int threads_per_block         = 256;
        const int work_items_per_thread     = 4;
        const int num_blocks                = 
            (numBins) / (work_items_per_thread * threads_per_block);
        const int shared_mem_per_block      = 
            (threads_per_block + log_num_banks) * 2 * sizeof(int);
        
        const dim3 grid_size(num_blocks);
        const dim3 block_size(threads_per_block);

        prefix_scan
        <
            threads_per_block,
            work_items_per_thread
        >
        <<<
            grid_size,
            block_size,
            shared_mem_per_block
        >>>
        (
            sym_histogram_bins, 
            sum_operator<int>(), 
            d_cdf
        );
    }

    cudaDeviceSynchronize();

  /****************************************************************************
  * You can use the code below to help with debugging, but make sure to       *
  * comment it out again before submitting your assignment for grading,       *
  * otherwise this code will take too much time and make it seem like your    *
  * GPU implementation isn't fast enough.                                     *
  *                                                                           *
  * This code generates a reference cdf on the host by running the            *
  * reference calculation we have given you.  It then copies your GPU         *
  * generated cdf back to the host and calls a function that compares the     *
  * the two and will output the first location they differ.                   *
  * ************************************************************************* */


    float *h_logLuminance = new float[numRows * numCols];
    unsigned int *h_cdf   = new unsigned int[numBins];
    unsigned int *h_your_cdf = new unsigned int[numBins];
    checkCudaErrors(cudaMemcpy(h_logLuminance, d_logLuminance, numCols * numRows * sizeof(float), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(h_your_cdf, d_cdf, numBins * sizeof(unsigned int), cudaMemcpyDeviceToHost));

    void write_luminance(const float* const, const int);

    write_luminance(h_logLuminance, static_cast<int>(numRows * numCols));
    
    referenceCalculation(h_logLuminance, h_cdf, numRows, numCols, numBins);

    //compare the results of the CDF
    checkResultsExact(h_cdf, h_your_cdf, numBins);
 
    delete[] h_logLuminance;
    delete[] h_cdf; 
    delete[] h_your_cdf; 
}

void write_luminance(const float* const lum_array, const int num_values) {
    struct FILE_Deleter {
        void operator()(FILE* fp) const {
            if (fp) {
                fclose(fp);
            }
        }
    };

    std::unique_ptr<FILE, FILE_Deleter> fp(fopen("C:\\temp\\lum.out", "wt"));
    if (!fp) {
        return;
    }

    for (int i = 0; i < num_values; ++i) {
        fprintf(fp.get(), "%.6f\n", lum_array[i]);
    }
}