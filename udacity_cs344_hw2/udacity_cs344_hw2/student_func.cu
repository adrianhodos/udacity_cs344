// Homework 2
// Image Blurring
//
// In this homework we are blurring an image. To do this, imagine that we have
// a square array of weight values. For each pixel in the image, imagine that we
// overlay this square array of weights on top of the image such that the center
// of the weight array is aligned with the current pixel. To compute a blurred
// pixel value, we multiply each pair of numbers that line up. In other words, we
// multiply each weight with the pixel underneath it. Finally, we add up all of the
// multiplied numbers and assign that value to our output for the current pixel.
// We repeat this process for all the pixels in the image.

// To help get you started, we have included some useful notes here.

//****************************************************************************

// For a color image that has multiple channels, we suggest separating
// the different color channels so that each color is stored contiguously
// instead of being interleaved. This will simplify your code.

// That is instead of RGBARGBARGBARGBA... we suggest transforming to three
// arrays (as in the previous homework we ignore the alpha channel again):
//  1) RRRRRRRR...
//  2) GGGGGGGG...
//  3) BBBBBBBB...
//
// The original layout is known an Array of Structures (AoS) whereas the
// format we are converting to is known as a Structure of Arrays (SoA).

// As a warm-up, we will ask you to write the kernel that performs this
// separation. You should then write the "meat" of the assignment,
// which is the kernel that performs the actual blur. We provide code that
// re-combines your blurred results for each color channel.

//****************************************************************************

// You must fill in the gaussian_blur kernel to perform the blurring of the
// inputChannel, using the array of weights, and put the result in the outputChannel.

// Here is an example of computing a blur, using a weighted average, for a single
// pixel in a small image.
//
// Array of weights:
//
//  0.0  0.2  0.0
//  0.2  0.2  0.2
//  0.0  0.2  0.0
//
// Image (note that we align the array of weights to the center of the box):
//
//    1  2  5  2  0  3
//       -------
//    3 |2  5  1| 6  0       0.0*2 + 0.2*5 + 0.0*1 +
//      |       |
//    4 |3  6  2| 1  4   ->  0.2*3 + 0.2*6 + 0.2*2 +   ->  3.2
//      |       |
//    0 |4  0  3| 4  2       0.0*4 + 0.2*0 + 0.0*3
//       -------
//    9  6  5  0  3  9
//
//         (1)                         (2)                 (3)
//
// A good starting place is to map each thread to a pixel as you have before.
// Then every thread can perform steps 2 and 3 in the diagram above
// completely independently of one another.

// Note that the array of weights is square, so its height is the same as its width.
// We refer to the array of weights as a filter, and we refer to its width with the
// variable filterWidth.

//****************************************************************************

// Your homework submission will be evaluated based on correctness and speed.
// We test each pixel against a reference solution. If any pixel differs by
// more than some small threshold value, the system will tell you that your
// solution is incorrect, and it will let you try again.

// Once you have gotten that working correctly, then you can think about using
// shared memory and having the threads cooperate to achieve better performance.

//****************************************************************************

// Also note that we've supplied a helpful debugging function called checkCudaErrors.
// You should wrap your allocation and copying statements like we've done in the
// code we're supplying you. Here is an example of the unsafe way to allocate
// memory on the GPU:
//
// cudaMalloc(&d_red, sizeof(unsigned char) * numRows * numCols);
//
// Here is an example of the safe way to do the same thing:
//
// checkCudaErrors(cudaMalloc(&d_red, sizeof(unsigned char) * numRows * numCols));
//
// Writing code the safe way requires slightly more typing, but is very helpful for
// catching mistakes. If you write code the unsafe way and you make a mistake, then
// any subsequent kernels won't compute anything, and it will be hard to figure out
// why. Writing code the safe way will inform you as soon as you make a mistake.

// Finally, remember to free the memory you allocate at the end of the function.

//****************************************************************************

#include <memory>
#include "reference_calc.cpp"
#include "utils.h"

const int kMaxFilterWidth = 15;

__constant__ float  kFilterMatrix[kMaxFilterWidth * kMaxFilterWidth];
__constant__ int    kIntFilterMatrix[kMaxFilterWidth * kMaxFilterWidth];

texture<uchar4, cudaTextureType2D, cudaReadModeElementType> texRefInputImage;

//
// Gaussian blur using an image stored in a 2D uchar4 texture. 
// Disappointing performance :( .
template<int FilterWidth>
__global__
void gaussian_blur_tex(uchar4* const outputChannel, const int numRows, const int numCols) {    
    //
    // Compute global positions along the X and Y axis.
    const int rowIdx = blockIdx.y * blockDim.y + threadIdx.y;
    const int colIdx = blockIdx.x * blockDim.x + threadIdx.x;

    //
    // Setup halo/ghost radius
    const int filterRadius = FilterWidth / 2;

    //
    // Accumulator for the filter.
    //float outputPixelValue = 0.0f;

    //
    // Apply filter for this element.
    float4 fx = { 0.0f, 0.0f, 0.0f, 255.0f };

#pragma unroll
    for (int filterRow = 0; filterRow < FilterWidth; ++filterRow) {
        const float yCoord = rowIdx - filterRadius + filterRow;
#pragma unroll
        for (int filterCol = 0; filterCol < FilterWidth; ++filterCol) {
            const float xCoord = colIdx - filterRadius + filterCol;

            const uchar4 inputPixel = tex2D(texRefInputImage, xCoord, yCoord);

            const float filterValue = kFilterMatrix[filterRow * FilterWidth + filterCol];
            fx.x += inputPixel.x * filterValue;
            fx.y += inputPixel.y * filterValue;
            fx.z += inputPixel.z * filterValue;
        }
    }

    //
    //  If the thread indices along x and y are within the image limits,
    //  we can write the filtered value into the output image.
    if ((rowIdx < numRows) && (colIdx < numCols)) {
        const int outputElemIdx = rowIdx * numCols + colIdx; 
        outputChannel[outputElemIdx].x = static_cast<unsigned char>(fx.x);
        outputChannel[outputElemIdx].y = static_cast<unsigned char>(fx.y);
        outputChannel[outputElemIdx].z = static_cast<unsigned char>(fx.z);
        outputChannel[outputElemIdx].w = static_cast<unsigned char>(fx.w);
    }
}

template<typename Vector3Type, typename Vector4Type>
__device__ inline Vector3Type Vector4ToVector3(const Vector4Type& inputVal) {
    Vector3Type outputValue = { inputVal.x, inputVal.y, inputVal.z };
    return outputValue;
}

template<int BlockSize, int FilterWidth>
__global__
void gaussian_blur(
    const uchar4* __restrict__ const inputChannel,
    uchar4* __restrict__ const outputChannel,
    int numRows, 
    int numCols
    ) {
    // TODO
  
    // NOTE: Be sure to compute any intermediate results in floating point
    // before storing the final result as unsigned char.

    // NOTE: Be careful not to try to access memory that is outside the bounds of
    // the image. You'll want code that performs the following check before accessing
    // GPU memory:
    //
    // if ( absolute_image_position_x >= numCols ||
    //      absolute_image_position_y >= numRows )
    // {
    //     return;
    // }
  
    // NOTE: If a thread's absolute position 2D position is within the image, but some of
    // its neighbors are outside the image, then you will need to be extra careful. Instead
    // of trying to read such a neighbor value from GPU memory (which won't work because
    // the value is out of bounds), you should explicitly clamp the neighbor values you read
    // to be within the bounds of the image. If this is not clear to you, then please refer
    // to sequential reference solution for the exact clamping semantics you should follow.

    const int tx = threadIdx.x;
    const int ty = threadIdx.y;

    //
    // Compute global positions along the X and Y axis.
    const int row = blockIdx.y * blockDim.y + threadIdx.y;
    const int col = blockIdx.x * blockDim.x + threadIdx.x;
    //
    // setup shared mem array
    __shared__ uchar3 imgBytes[BlockSize + FilterWidth - 1][BlockSize + FilterWidth];

    //
    // Setup halo/ghost radius
    const int filterRadius = FilterWidth / 2;

    //
    // Clamped pixel coordinates in the input image (x left, x right, y up, y bottom)
    int loadCoords[4];

    //
    // Clamp row address to image boundary.
    const int rowAddress = min(row, numRows - 1);
    //const int rowAddress = row;

    //
    // Check if thread X coord is in ghost/halo radius.
    if (tx < filterRadius) {
        //
        // Load left side ghost/halo.
        loadCoords[0] = min(max(col - filterRadius, 0), numCols - 1);
        imgBytes[ty + filterRadius][tx] = Vector4ToVector3<uchar3>(
            inputChannel[rowAddress * numCols + loadCoords[0]]);
        
        //
        // Load right side ghost/halo
        loadCoords[1] = min(max(col + BlockSize, 0), numCols - 1);
        imgBytes[ty + filterRadius][tx + BlockSize + filterRadius] = 
            Vector4ToVector3<uchar3>(inputChannel[rowAddress * numCols + loadCoords[1]]);
    }

    //
    // Clamp column address to be within image boundary.
    const int columnAddress = min(col, numCols - 1);
    if (ty < filterRadius) {
        //
        // Load upper ghost/halo.
        loadCoords[2] = min(max(row - filterRadius, 0), numRows - 1);
        imgBytes[ty][tx + filterRadius] = Vector4ToVector3<uchar3>(
            inputChannel[loadCoords[2] * numCols + columnAddress]);

        //
        // Load lower ghost/halo.
        loadCoords[3] = min(max(row + BlockSize, 0), numRows - 1);
        imgBytes[ty + filterRadius + BlockSize][tx + filterRadius] = 
            Vector4ToVector3<uchar3>(inputChannel[loadCoords[3] * numCols + columnAddress]);
    }
    
    //
    // Load diagonal ghost/halo.
    if ((tx < filterRadius) && (ty < filterRadius)) {
        //
        // (y0, x0)
        imgBytes[ty][tx] = Vector4ToVector3<uchar3>(
            inputChannel[loadCoords[2] * numCols + loadCoords[0]]);
        //
        // (y0, x1)
        imgBytes[ty][tx + filterRadius + BlockSize] =
            Vector4ToVector3<uchar3>(inputChannel[loadCoords[2] * numCols + loadCoords[1]]);
            
        //
        // (y1, x0)
        imgBytes[ty + filterRadius + BlockSize][tx] = 
            Vector4ToVector3<uchar3>(inputChannel[loadCoords[3] * numCols + loadCoords[0]]);
            
        //
        // (y1, x1)
        imgBytes[ty + filterRadius + BlockSize][tx + filterRadius + BlockSize] = 
            Vector4ToVector3<uchar3>(inputChannel[loadCoords[3] * numCols + loadCoords[1]]);
    }

    //
    //  Load cell corresponding to this thread, into shared memory.
    imgBytes[ty + filterRadius][tx + filterRadius] = 
        Vector4ToVector3<uchar3>(inputChannel[rowAddress * numCols + columnAddress]);

    //
    //  Synchronize threads so that image data is fully loaded.
    __syncthreads();

    if ((row >= numRows) || (col >= numCols)) {
        return;
    }

    //
    // Accumulator for the filter.
    //float3 outputPixelValue = { 0.0f, 0.0f, 0.0f };
    int3 accumPixelValue = { 0, 0, 0 };

    //
    // Apply filter for this element.
#pragma unroll
    for (int filterRow = 0; filterRow < FilterWidth; ++filterRow) {
#pragma unroll
        for (int filterCol = 0; filterCol < FilterWidth; ++filterCol) {
            const int imgRowIdx             = ty + filterRow;
            const int imgColIdx             = tx + filterCol;
            const int filterElemIdx         = filterRow * FilterWidth + filterCol;
            const uchar3& inputPixelValue   = imgBytes[imgRowIdx][imgColIdx]; 
            const int kFilterVal            = kIntFilterMatrix[filterElemIdx];
            accumPixelValue.x              += inputPixelValue.x * kFilterVal;
            accumPixelValue.y              += inputPixelValue.y * kFilterVal;
            accumPixelValue.z              += inputPixelValue.z * kFilterVal;
        }
    }

    uchar4 outputPixelValue = make_uchar4(
        (unsigned char) (accumPixelValue.x / (2 << 20)),
        (unsigned char) (accumPixelValue.y / (2 << 20)),
        (unsigned char) (accumPixelValue.z / (2 << 20)),
        255
    );

    outputChannel[row * numCols + col] = outputPixelValue;
}

//This kernel takes in an image represented as a uchar4 and splits
//it into three images consisting of only one color channel each
__global__
void separateChannels(const uchar4* const inputImageRGBA,
                      int numRows,
                      int numCols,
                      unsigned char* const redChannel,
                      unsigned char* const greenChannel,
                      unsigned char* const blueChannel)
{
  // TODO
  //
  // NOTE: Be careful not to try to access memory that is outside the bounds of
  // the image. You'll want code that performs the following check before accessing
  // GPU memory:
  //
  // if ( absolute_image_position_x >= numCols ||
  //      absolute_image_position_y >= numRows )
  // {
  //     return;
  // }
    const int columnId      = blockIdx.x * blockDim.x + threadIdx.x;
    const int rowIdx        = blockIdx.y * blockDim.y + threadIdx.y;

    if ((columnId < numCols) && (rowIdx < numRows)) {
        const int element_addr  = rowIdx * numCols + columnId;
        const uchar4 in_element = inputImageRGBA[element_addr];

        redChannel[element_addr]    = in_element.x;
        greenChannel[element_addr]  = in_element.y;
        blueChannel[element_addr]   = in_element.z;
    }
}

//This kernel takes in three color channels and recombines them
//into one image.  The alpha channel is set to 255 to represent
//that this image has no transparency.
__global__
void recombineChannels(const unsigned char* const redChannel,
                       const unsigned char* const greenChannel,
                       const unsigned char* const blueChannel,
                       uchar4* const outputImageRGBA,
                       int numRows,
                       int numCols)
{
  const int2 thread_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

  const int thread_1D_pos = thread_2D_pos.y * numCols + thread_2D_pos.x;

  //make sure we don't try and access memory outside the image
  //by having any threads mapped there return early
  if (thread_2D_pos.x >= numCols || thread_2D_pos.y >= numRows)
    return;

  unsigned char red   = redChannel[thread_1D_pos];
  unsigned char green = greenChannel[thread_1D_pos];
  unsigned char blue  = blueChannel[thread_1D_pos];

  //Alpha should be 255 for no transparency
  uchar4 outputPixel = make_uchar4(red, green, blue, 255);

  outputImageRGBA[thread_1D_pos] = outputPixel;
}

unsigned char *d_red, *d_green, *d_blue;
float         *d_filter;

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{

  //allocate memory for the three different channels
  //original
  /*checkCudaErrors(cudaMalloc(&d_red,   sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_green, sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_blue,  sizeof(unsigned char) * numRowsImage * numColsImage));*/

  //TODO:
  assert(filterWidth <= kMaxFilterWidth);

  const int kFilterSize = static_cast<int>(filterWidth * filterWidth);
  std::unique_ptr<int[]> filterAsInteger(new int[kFilterSize]);
  int* outputPtr = filterAsInteger.get();
  for (int idx = 0; idx < kFilterSize; ++idx) {
      outputPtr[idx] = static_cast<int>(h_filter[idx] * (2 << 20));
  }
  cudaMemcpyToSymbol(kIntFilterMatrix, outputPtr, kFilterSize * sizeof(int));

  //Allocate memory for the filter on the GPU
  //Use the pointer d_filter that we have already declared for you
  //You need to allocate memory for the filter with cudaMalloc
  //be sure to use checkCudaErrors like the above examples to
  //be able to tell if anything goes wrong
  //IMPORTANT: Notice that we pass a pointer to a pointer to cudaMalloc

  //TODO:
  //Copy the filter on the host (h_filter) to the memory you just allocated
  //on the GPU.  cudaMemcpy(dst, src, numBytes, cudaMemcpyHostToDevice);
  //Remember to use checkCudaErrors!
}

struct CudaArrayDeleter {
    void operator()(cudaArray* arrayPtr) const {
        cudaFreeArray(arrayPtr);
    }
};

///////////////////////////////////////////////////////////////////////////////
void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  //TODO: Set reasonable block size (i.e., number of threads per block)
    const int NumThreadsPerBlock = 16;
    const dim3 blockSize(NumThreadsPerBlock, NumThreadsPerBlock, 1);

    //TODO:
    //Compute correct grid size (i.e., number of blocks per kernel launch)
    //from the image size and and block size.
    const dim3 gridSize(((int) numCols * 2 - 1) / NumThreadsPerBlock + 1,
                        ((int) numRows - 1) / NumThreadsPerBlock + 1);

    switch (filterWidth) {
    case 15 :
        gaussian_blur<NumThreadsPerBlock, 15><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 13 :
        gaussian_blur<NumThreadsPerBlock, 13><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 11 :
        gaussian_blur<NumThreadsPerBlock, 11><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 9 :
        gaussian_blur<NumThreadsPerBlock, 9><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 7 :
        gaussian_blur<NumThreadsPerBlock, 7><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 5 :
        gaussian_blur<NumThreadsPerBlock, 5><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    case 3 :
        gaussian_blur<NumThreadsPerBlock, 3><<<gridSize, blockSize>>>(
            d_inputImageRGBA, d_outputImageRGBA, static_cast<int>(numRows), 
            static_cast<int>(numCols));
        break;

    default :
        assert(false && "Unsupported filter width!");
        break;
    }

    // Again, call cudaDeviceSynchronize(), then call checkCudaErrors() immediately after
    // launching your kernel to make sure that you didn't make any mistakes.
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

    // Now we recombine your results. We take care of launching this kernel for you.
    //
    // NOTE: This kernel launch depends on the gridSize and blockSize variables,
    // which you must set yourself.
    /*recombineChannels<<<gridSize, blockSize>>>(d_redBlurred,
                                                d_greenBlurred,
                                                d_blueBlurred,
                                                d_outputImageRGBA,
                                                static_cast<int>(numRows),
                                                static_cast<int>(numCols));
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());*/

    /****************************************************************************
    * You can use the code below to help with debugging, but make sure to       *
    * comment it out again before submitting your assignment for grading,       *
    * otherwise this code will take too much time and make it seem like your    *
    * GPU implementation isn't fast enough.                                     *
    *                                                                           *
    * This code generates a reference image on the host by running the          *
    * reference calculation we have given you.  It then copies your GPU         *
    * generated image back to the host and calls a function that compares the   *
    * the two and will output the first location they differ by too much.       *
    * ************************************************************************* */

    uchar4 *h_outputImage     = new uchar4[numRows * numCols];
    uchar4 *h_outputReference = new uchar4[numRows * numCols];

    checkCudaErrors(cudaMemcpy(h_outputImage, d_outputImageRGBA, 
                                numRows * numCols * sizeof(uchar4), 
                                cudaMemcpyDeviceToHost));

    extern float *h_filter__;
    referenceCalculation(h_inputImageRGBA, h_outputReference, numRows, numCols,
                         h_filter__, filterWidth);

    //the 4 is because there are 4 channels in the image
    /*checkResultsExact((unsigned char *)h_outputReference,
                    (unsigned char *)h_outputImage,
                    numRows * numCols * 4); */
    //
    // Must use this version, since the kernel uses integer arithmetic, thus
    // causing some small differences with the reference image.
    checkResultsEps((unsigned char*) h_outputReference, (unsigned char*) h_outputImage,
                    numRows * numCols * 4, 1.0f, 2.0f);
 
    delete [] h_outputImage;
    delete [] h_outputReference;
}

///////////////////////////////////////////////////////////////////////////////


//Free all the memory that we allocated
//TODO: make sure you free any arrays that you allocated
void cleanup() {
  /*checkCudaErrors(cudaFree(d_red));
  checkCudaErrors(cudaFree(d_green));
  checkCudaErrors(cudaFree(d_blue));*/
}
